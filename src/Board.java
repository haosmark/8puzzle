import java.util.Arrays;

import static java.lang.Math.abs;

public class Board {
    private byte[] board;
    private int dimension;
    private Stack<Board> stack;
    private int manhattanRank;
    private int hammingRank;

    /**
     * construct a board from an N-by-N array of blocks (where blocks[i][j] = block in row i, column j)
     * @param blocks two-dimensional array containing grid data
     */
    public Board(int[][] blocks) {
        dimension = blocks.length;
        board = new byte[dimension * dimension];
        int index = 0;
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                board[index++] = (byte) blocks[i][j];
            }
        }
        manhattanRank = manhattan();
        hammingRank = hamming();
    }

    private Board(byte[] blocks, int dimension) {
        this.dimension = dimension;
        board = new byte[dimension * dimension];
        System.arraycopy(blocks, 0, board, 0, blocks.length);
        manhattanRank = manhattan();
        hammingRank = hamming();
    }

    /**
     * board dimension N
     * @return board size (3 x 3 board has a size of 3)
     */
    public int dimension() {
        return dimension;
    }

    /**
     * @return number of blocks out of place
     */
    public int hamming() {
        if (hammingRank != 0) {     // caching
            return hammingRank;
        }
        int rank = 0;
        for (int i = 1; i <= board.length; i++) {
            if (board[i - 1] != i && board[i - 1] != 0) { // if block is out of place, increase rank
                rank++;
            }
        }
        return rank;
    }

    /**
     * @return sum of Manhattan distances between blocks and goal
     */
    public int manhattan() {
        if (manhattanRank != 0) {      // caching
            return manhattanRank;
        }
        int rank = 0;
        for (int i = 1; i <= board.length; i++) {
            if (board[i - 1] != i && board[i - 1] != 0) { // if block is out of place
                byte[] rcActual = decodeXY(board[i - 1]);
                byte[] rcExpected = decodeXY(i);
                rank += (abs(rcActual[0] - rcExpected[0]) + abs(rcActual[1] - rcExpected[1]));
            }
        }
        return rank;
    }

    /**
     * @return true if it's the goal board, false otherwise
     */
    public boolean isGoal() {
        return board[board.length - 1] == 0 && manhattan() == 0;
    }

    /**
     *
     * @return a board that is obtained by exchanging two adjacent blocks in the same row
     */
    public Board twin() {
        Board copy;
        int[] index = getBestTwin();
        if (index[0] == 0) {
            if (board[0] != 0 && board[1] != 0) {
                swap(0, 1);
                copy = new Board(board, this.dimension);
                swap(0, 1);
            } else {
                swap(dimension, dimension + 1);
                copy = new Board(board, this.dimension);
                swap(dimension, dimension + 1);
            }
        } else {
            swap(index[0], index[1]);
            copy = new Board(board, this.dimension);
            swap(index[0], index[1]);
        }
        return copy;
    }

    /**
     *
     * @return the preferred blocks to swap, 0 and 1 are default.
     */
    private int[] getBestTwin() {
        // collect indexes of all blocks that are out of place
        for (int i = 0; i < board.length; i++) {
            if (board[i] != 0 && board[i] != i + 1 && i + 1 < board.length) {
                // check if adjacent item is also out of place (potential swap pair)
                if (board[i + 1] != i + 2) {
                    // if blocks are in the same row
                    if (Math.ceil((i + 1.0) / dimension) == Math.ceil((i + 2.0) / dimension)) {
                        // if shifting blocks gives advantage with hamming score, then use them as preferred
                        if (board[i + 1] == i + 1 || board[i] == i + 2) {
                            return new int[] {i, i + 1};
                        }
                    }
                }
            }
        }
        return new int[] {0, 1};
    }

    private void swap(int a, int b) {
        byte temp = board[a];
        board[a] = board[b];
        board[b] = temp;
    }

    /**
     * @param y board to compare to
     * @return true if this board equals Y, false otherwise
     */
    public boolean equals(Object y) {
        if (y == null) {
            return false;
        }
        if (this == y) {
            return true;
        }
        if (this.getClass() != y.getClass()) {
            return false;
        }
        Board that = (Board) y;
        return Arrays.equals(this.board, that.board);
    }

    /**
     * @return all neighboring boards
     */
    public Iterable<Board> neighbors() {
        stack = new Stack<Board>();
        byte emptyIndex = 0;
        // find empty cell
        for (byte i = 0; i < board.length; i++) {
            if (board[i] == 0) {
                emptyIndex = i;
                break;
            }
        }
        byte[] rc = decodeXY(emptyIndex + 1);
        byte row = rc[0];
        byte column = rc[1];

        if (column > 0) { // left
            swap(emptyIndex, emptyIndex - 1);
            stack.push(new Board(board, this.dimension));
            swap(emptyIndex, emptyIndex - 1);
        }
        if (row > 0) { // top
            swap(emptyIndex, emptyIndex - dimension);
            stack.push(new Board(board, this.dimension));
            swap(emptyIndex, emptyIndex - dimension);
        }
        if (column < dimension() - 1) { // right
            swap(emptyIndex, emptyIndex + 1);
            stack.push(new Board(board, this.dimension));
            swap(emptyIndex, emptyIndex + 1);
        }
        if (row < dimension() - 1) { // bottom
            swap(emptyIndex, emptyIndex + dimension);
            stack.push(new Board(board, this.dimension));
            swap(emptyIndex, emptyIndex + dimension);
        }
        return stack;
    }

    /**
     * @param i row of the board
     * @param j column of the board
     * @return correct number that should correspond with the cell
     */
    private int encodeXY(int i, int j) {
        return i * dimension() + j + 1;
    }

    /**
     *
     * @param index of the cell
     * @return an array with row and column at 0 and 1 respectively
     */
    private byte[] decodeXY(int index) {
        byte column = (byte)((index - 1) % dimension());
        byte row = (byte)((index - column) / dimension());
        return new byte[] {row, column};
    }

    /**
     * @return string representation of this board
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(dimension());
        for (int i = 0; i < board.length; i++) {
            if (i % dimension == 0) {
                builder.append("\n");
            }
            builder.append(String.format("%2d ", board[i]));
        }
        builder.append("\n");
        return builder.toString();
    }

    // unit tests (not graded)
    public static void main(String[] args) {
        int[][] blocks = {
                {0, 1, 3},
                {4, 2, 5},
                {7, 8, 6}
        };
        Board board = new Board(blocks);
        System.out.println(board.toString());
    }
}