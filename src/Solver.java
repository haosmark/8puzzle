public class Solver {
    private MinPQ<SearchNode> pq;
    private Stack<Board> solutionPath = new Stack<Board>();
    private boolean solvable = true;
    private int moveCounter = 0;

    private class SearchNode implements Comparable<SearchNode> {
        Board board;
        private int moves;
        SearchNode parent;

        public SearchNode(Board board) {
            this.board = board;
            this.moves = 0;
            parent = null;
        }
        public SearchNode(Board board, int moves, SearchNode parent) {
            this.board = board;
            this.moves = moves;
            this.parent = parent;
        }

        public int getPriority() {
            return this.board.manhattan() + this.moves;
        }

        @Override
        public int compareTo(SearchNode that) {
            return this.getPriority() - that.getPriority();
        }
    }

    /**
     * find a solution to the initial board (using the A* algorithm)
     * @param initial
     */
    public Solver(Board initial) {
//        Stopwatch watch = new Stopwatch();
        if (initial == null) {
            throw new java.lang.NullPointerException();
        }
        pq = new MinPQ<SearchNode>();
        SearchNode minNode = new SearchNode(initial);
        SearchNode twinMinNode = new SearchNode(initial.twin());

        while (true) {
            moveCounter++;
            minNode = processNeighbor(minNode);
            if (minNode.board.isGoal()) {
                getSolution(minNode, initial.twin());
                return;
            }
            twinMinNode = processNeighbor(twinMinNode);
            if (twinMinNode.board.isGoal()) {
                getSolution(twinMinNode, initial.twin());
                return;
            }
        }
//        System.out.println("time to solve the puzzle: " + watch.elapsedTime());
    }

    private SearchNode processNeighbor(SearchNode minNode) {
        for (Board neighbor : minNode.board.neighbors()) {
            if (minNode.parent == null || !neighbor.equals(minNode.parent.board)) {
                pq.insert(new SearchNode(neighbor, moveCounter, minNode));
            }
        }
        return pq.delMin();
    }

    private void getSolution(SearchNode minNode, Board twin) {
        while (minNode != null) {
            solutionPath.push(minNode.board);
            if (minNode.parent == null) {                  // check if originating node is twin or not
                if (minNode.board.equals(twin)) {          // if this is a twin, then you can't solve this puzzle
                    solvable = false;
                }
            }
            minNode = minNode.parent;
        }
    }

    /**
     * @return true if the initial board is solvable
     */
    public boolean isSolvable() {
        return solvable;
    }

    /**
     * @return min number of moves to solve initial board; -1 if unsolvable
     */
    public int moves() {
        return solutionPath.size() - 1;
    }

    /**
     * @return sequence of boards in a shortest solution; null if unsolvable
     */
    public Iterable<Board> solution() {
        return (isSolvable()) ? solutionPath : null;
    }

    public static void main(String[] args) {

        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}
