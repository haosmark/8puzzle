import org.junit.Test;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BoardTests {
    @Test
    public void testIsGoalTrueFalse() {
        int[][] blocks = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 0}
        };
        Board board = new Board(blocks);
        assertThat(board.isGoal(), is(true));

        blocks[0][0] = 0;
        blocks[2][2] = 1;
        board = new Board(blocks);
        assertThat(board.isGoal(), is(false));
    }

    @Test
    public void hammingShouldReturnFive() {
        int[][] blocks = {
                {8, 1, 3},
                {4, 0, 2},
                {7, 6, 5}
        };
        Board board = new Board(blocks);
        assertThat(board.hamming(), is(equalTo(5)));
    }

    @Test
    public void manhattanShouldReturnTen() {
        int[][] blocks = {
                {8, 1, 3},
                {4, 0, 2},
                {7, 6, 5}
        };
        Board board = new Board(blocks);
        assertThat(board.manhattan(), is(equalTo(10)));
    }

    @Test
    public void testEquals() {
        int[][] blocks = {
                {8, 1, 3},
                {4, 0, 2},
                {7, 6, 5}
        };
        int[][] blocks2 = {
                {1, 2, 3},
                {4, 0, 8},
                {7, 6, 5}
        };
        Board board = new Board(blocks);
        Board board2 = new Board(blocks2);

        assertThat(board.equals(board2), is(false));
        board2 = new Board(blocks);
        assertThat(board.equals(board2), is(true));
    }

    @Test
    public void testNeighbors() {
        int[][] blocks = {
                {0, 1, 3},
                {4, 2, 5},
                {7, 8, 6}
        };
        Board board = new Board(blocks);
        Iterable<Board> neighbors = board.neighbors();

        int i = 0;
        for (Board neighbor : neighbors) {
            if (i == 0) {
                assertThat(neighbor.toString(), is(neighbor.dimension() + "\n 4  1  3 \n 0  2  5 \n 7  8  6 \n"));
            } else {
                assertThat(neighbor.toString(), is(neighbor.dimension() + "\n 1  0  3 \n 4  2  5 \n 7  8  6 \n"));
            }
            i++;
        }
    }
}
